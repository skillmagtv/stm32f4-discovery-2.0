/**
 * @file my_library.c
 * @author Nicolas Murcia Orjuela (nicolas.murciao@autonoma.edu.co)
 * @brief this is a library with functions and definitions used to control the LEDs and blue button integrated in the STM32F429IDISC board.
 * @version 1.0
 * @date 2023-05-18
 * @copyright Copyright (c) 2023
 */

/* Includes-----------------------------------------------------------------------*/
#include "my_library.h"

/* Local Variables declaration--------------------------------------------------*/
uint32_t start_tick;
bool led_switch;

/**
 * @brief this function initializes the GPIO pins of the STM32F429IDISC1 board.
 * 
 */
void my_gpio_init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOG, GREEN_LED_PIN|RED_LED_PIN, GPIO_PIN_RESET);

  /*Configure GPIO pin : BLUE_BUTTON_Pin */
  GPIO_InitStruct.Pin = BLUE_BUTTON_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(BLUE_BUTTON_GPIO_PORT, &GPIO_InitStruct);

  /*Configure GPIO pins : GREEN_LED_Pin RED_LED_Pin */
  GPIO_InitStruct.Pin = GREEN_LED_PIN|RED_LED_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);
}

/**
 * @brief Reads the state of the blue button.
 * @return Returns true if the blue button is pressed, or false if it is not pressed.
 */
bool read_blue_button(void)
{
    GPIO_PinState state = HAL_GPIO_ReadPin(BLUE_BUTTON_GPIO_PORT, BLUE_BUTTON_PIN);
    return (state == GPIO_PIN_SET);
}

/**
 * @brief Performs an LED animation with alternating colors.
 *
 * This function controls the LEDs (green and red) to create an animation with alternating colors.
 * The LED colors are switched every 200 milliseconds.
 */
void led_animation(void)
{
    if (led_switch)
    {
    	HAL_GPIO_WritePin(GREEN_LED_GPIO_PORT, GREEN_LED_PIN, GPIO_PIN_SET);
    	HAL_GPIO_WritePin(RED_LED_GPIO_PORT, RED_LED_PIN, GPIO_PIN_RESET);
    }
    else
    {
    	HAL_GPIO_WritePin(RED_LED_GPIO_PORT, RED_LED_PIN, GPIO_PIN_SET);
    	HAL_GPIO_WritePin(GREEN_LED_GPIO_PORT, GREEN_LED_PIN, GPIO_PIN_RESET);
    }
    if (HAL_GetTick() - start_tick >= 200)
	{
    	led_switch = !led_switch;
    	start_tick = HAL_GetTick();
	}
}

/**
 * @brief Resets the state of the LEDs.
 *
 * This function turns off both the green and red LEDs by setting their corresponding GPIO pins to a low state.
 */
void led_reset(void)
{
	  HAL_GPIO_WritePin(GREEN_LED_GPIO_PORT, GREEN_LED_PIN, GPIO_PIN_RESET);
	  HAL_GPIO_WritePin(RED_LED_GPIO_PORT, RED_LED_PIN, GPIO_PIN_RESET);
}

/**
 * @brief Initializes the timer for LED animation.
 *
 * This function initializes the timer by setting the start tick value to the current system tick obtained using the `HAL_GetTick` function.
 * It also sets the LED switch state to false, indicating the initial LED color.
 */
void timer_init(void)
{
	start_tick = HAL_GetTick();
	led_switch = false;
}
/*********************************END OF FILE*************************************/
