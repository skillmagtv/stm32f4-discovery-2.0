/**
 * @file my_library.h
 * @author Nicolas Murcia Orjuela (nicolas.murciao@autonoma.edu.co)
 * @brief this file contains definitions and includes needed for the library.
 * @version 1.0
 * @date 2023-05-18
 * @copyright Copyright (c) 2023
 */

/* Define to prevent recursive inclusion------------------------------------------*/
#ifndef INC_MY_LIBRARY_H_
#define INC_MY_LIBRARY_H_

/* Includes-----------------------------------------------------------------------*/
#include <stdbool.h>
#include "stdio.h"
#include "stm32f4xx.h"

/* Exported defines---------------------------------------------------------------*/
#define BLUE_BUTTON_PIN GPIO_PIN_0
#define BLUE_BUTTON_GPIO_PORT GPIOA
#define GREEN_LED_PIN GPIO_PIN_13
#define GREEN_LED_GPIO_PORT GPIOG
#define RED_LED_PIN GPIO_PIN_14
#define RED_LED_GPIO_PORT GPIOG

/* Exported functions-------------------------------------------------------------*/
void print_welcome_message(void);
bool read_blue_button(void);
void led_animation(void);
void led_reset(void);
void MY_GPIO_Init(void);
void timer_init(void);

#endif /* INC_MY_LIBRARY_H_ */
/*********************************END OF FILE*************************************/
